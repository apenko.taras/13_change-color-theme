if (localStorage.getItem("style") === null || localStorage.getItem("style") === 'main'){
    document.getElementById('pagestyle').setAttribute('href', './styles/main.css')
    localStorage["style"] = "main";
}else {
    document.getElementById('pagestyle').setAttribute('href', './styles/mainChangeColor.css')
}

document.querySelector('#btnChangeColor').onclick = function (){
    if (localStorage["style"] === 'mainChangeColor'){
        document.getElementById('pagestyle').setAttribute('href', './styles/main.css')
        localStorage["style"] = "main";
    }else {
        document.getElementById('pagestyle').setAttribute('href', './styles/mainChangeColor.css')
        localStorage["style"] = "mainChangeColor";
    }
}
